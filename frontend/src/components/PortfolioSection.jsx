import { SimpleGrid, Box, Image, Text } from '@chakra-ui/react';

export const PortfolioSection = () => {
  return (
    <Box py={{ base: '50px', md: '100px' }} px={{ base: '5%', md: '10%' }} bgGradient='linear(to-r, gray.200, gray.500)'>
      <Text fontSize={{ base: '24px', md: '40px', lg: '56px' }} mb='40px' textAlign='center' fontWeight='bold' color='white'>Marko Kraemer's Portfolio</Text>
      <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing='40px'>
        <Box bg='white' borderRadius='lg' overflow='hidden'>
          <Image src='https://picsum.photos/400/300' alt='Portfolio item' />
          <Text p='4' fontWeight='semibold'>Project 1</Text>
        </Box>
        <Box bg='white' borderRadius='lg' overflow='hidden'>
          <Image src='https://picsum.photos/400/300' alt='Portfolio item' />
          <Text p='4' fontWeight='semibold'>Project 2</Text>
        </Box>
        <Box bg='white' borderRadius='lg' overflow='hidden'>
          <Image src='https://picsum.photos/400/300' alt='Portfolio item' />
          <Text p='4' fontWeight='semibold'>Project 3</Text>
        </Box>
        <Box bg='white' borderRadius='lg' overflow='hidden'>
          <Image src='https://picsum.photos/400/300' alt='Portfolio item' />
          <Text p='4' fontWeight='semibold'>Project 4</Text>
        </Box>
        <Box bg='white' borderRadius='lg' overflow='hidden'>
          <Image src='https://picsum.photos/400/300' alt='Portfolio item' />
          <Text p='4' fontWeight='semibold'>Project 5</Text>
        </Box>
        <Box bg='white' borderRadius='lg' overflow='hidden'>
          <Image src='https://picsum.photos/400/300' alt='Portfolio item' />
          <Text p='4' fontWeight='semibold'>Project 6</Text>
        </Box>
      </SimpleGrid>
    </Box>
  );
};