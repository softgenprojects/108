import { Stack, Text, Image } from '@chakra-ui/react';

export const AboutSection = () => {
  return (
    <Stack spacing={8} direction={{ base: 'column', md: 'row' }} align='center' justify='center' p={10}>
      <Image
        borderRadius='full'
        boxSize={{ base: '150px', md: '200px' }}
        src='https://i.pravatar.cc/200'
        alt='Marko Kraemer'
      />
      <Stack spacing={4} maxW={{ base: '90%', md: '50%' }}>
        <Text fontSize={{ base: 'xl', md: '2xl' }} fontWeight='bold'>Marko Kraemer</Text>
        <Text fontSize={{ base: 'md', md: 'lg' }}>
          A passionate software engineer with a focus on creating elegant and efficient digital solutions. With a background in both design and development, Marko excels at bridging the gap between creative concepts and technical execution.
        </Text>
        <Text fontSize={{ base: 'sm', md: 'md' }}>
          Specialties include: React, Next.js, UI/UX Design, and Frontend Development.
        </Text>
      </Stack>
    </Stack>
  );
};