import { Box, Text, Button, useBreakpointValue } from '@chakra-ui/react';
import { useSmoothScroll } from '@hooks/useSmoothScroll';

export const HeroSection = () => {
  const scrollIntoView = useSmoothScroll('AboutSection', 700);
  const bgImage = useBreakpointValue({
    base: 'https://picsum.photos/480/720',
    md: 'https://picsum.photos/768/1024',
    lg: 'https://picsum.photos/1920/1080'
  });

  return (
    <Box
      w='full'
      h='100vh'
      bgImage={`url(${bgImage})`}
      bgAttachment='fixed'
      bgPos='center'
      bgSize='cover'
      display='flex'
      flexDirection='column'
      justifyContent='center'
      alignItems='center'
      textAlign='center'
      color='white'
      bgGradient='linear(to-r, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.1))'
    >
      <Text fontSize={{ base: '24px', md: '40px', lg: '56px' }} fontWeight='bold'>
        Welcome to Marko Kraemer's Portfolio
      </Text>
      <Text fontSize={{ base: '16px', md: '20px', lg: '24px' }} my={4}>
        Discover My World of Innovation and Design
      </Text>
      <Button
        size='lg'
        colorScheme='teal'
        variant='solid'
        onClick={scrollIntoView}
      >
        Explore My Work
      </Button>
    </Box>
  );
};