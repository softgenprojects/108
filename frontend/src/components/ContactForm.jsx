import { Button, FormControl, FormLabel, Input, Textarea, useBreakpointValue } from '@chakra-ui/react';

export const ContactForm = () => {
  const buttonSize = useBreakpointValue({ base: 'md', md: 'lg' });

  return (
    <FormControl padding={{ base: '4', md: '8' }} maxW={{ base: '90%', md: '500px' }} marginX='auto' bg='white' borderRadius='lg' boxShadow='xl' p={5}>
      <FormLabel htmlFor='name'>Name</FormLabel>
      <Input id='name' type='text' mb={4} />
      <FormLabel htmlFor='email'>Email</FormLabel>
      <Input id='email' type='email' mb={4} />
      <FormLabel htmlFor='message'>Message</FormLabel>
      <Textarea id='message' mb={4} />
      <Button size={buttonSize} colorScheme='blue' type='submit'>Submit</Button>
    </FormControl>
  );
};