import React, { useEffect, useState } from 'react';
import { Flex, Link, IconButton, useBreakpointValue } from '@chakra-ui/react';
import { HamburgerIcon } from '@chakra-ui/icons';
import { useSmoothScroll } from '@hooks/useSmoothScroll';

export const NavigationBar = () => {
  const [bgColor, setBgColor] = useState('transparent');
  const scrollToSection = useSmoothScroll;
  const isMobile = useBreakpointValue({ base: true, md: false });

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 50) {
        setBgColor('whiteAlpha.800');
      } else {
        setBgColor('transparent');
      }
    };

    window.addEventListener('scroll', handleScroll);

    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  return (
    <Flex
      as='nav'
      align='center'
      justify='space-between'
      wrap='wrap'
      padding='1.5rem'
      bg={bgColor}
      color='black'
      boxShadow={bgColor === 'transparent' ? 'none' : 'sm'}
      position='fixed'
      top='0'
      w='full'
      zIndex='sticky'
      transition='background-color 0.5s ease'
    >
      {isMobile ? (
        <IconButton
          aria-label='Open Menu'
          icon={<HamburgerIcon />}
          variant='outline'
          onClick={() => console.log('Open menu')}
        />
      ) : (
        <Flex align='center' mr={5}>
          <Link href='#' padding='1rem' onClick={scrollToSection('home', 500)}>Home</Link>
          <Link href='#' padding='1rem' onClick={scrollToSection('about', 500)}>About</Link>
          <Link href='#' padding='1rem' onClick={scrollToSection('portfolio', 500)}>Portfolio</Link>
          <Link href='#' padding='1rem' onClick={scrollToSection('contact', 500)}>Contact</Link>
        </Flex>
      )}
    </Flex>
  );
};