import { smoothScroll } from '@utilities/SmoothScrollUtils';

export const useSmoothScroll = (targetId, duration = 500) => {
  return () => {
    const targetElement = document.getElementById(targetId);
    if (targetElement) {
      const targetY = targetElement.offsetTop;
      smoothScroll(targetY, duration);
    }
  };
};