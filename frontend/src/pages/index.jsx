import { Box, VStack } from '@chakra-ui/react';
import { NavigationBar } from '@components/NavigationBar';
import { HeroSection } from '@components/HeroSection';
import { AboutSection } from '@components/AboutSection';
import { PortfolioSection } from '@components/PortfolioSection';
import { ContactForm } from '@components/ContactForm';
import { useSmoothScroll } from '@hooks/useSmoothScroll';

const Home = () => {
  const scrollToSection = useSmoothScroll;

  return (
    <Box minH='100vh' bgGradient='linear(to-b, gray.50, gray.200)'>
      <NavigationBar />
      <VStack spacing={{ base: 8, md: 10 }} align='stretch' justify='center' p={{ base: 4, lg: 8 }}>
        <Box id='home' onClick={scrollToSection('home', 500)}>
          <HeroSection />
        </Box>
        <Box id='about' onClick={scrollToSection('about', 500)}>
          <AboutSection />
        </Box>
        <Box id='portfolio' onClick={scrollToSection('portfolio', 500)}>
          <PortfolioSection />
        </Box>
        <Box id='contact' onClick={scrollToSection('contact', 500)}>
          <ContactForm />
        </Box>
      </VStack>
    </Box>
  );
};

export default Home;